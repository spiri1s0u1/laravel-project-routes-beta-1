
use App\Http\Controllers\Api\BibleautequeBDMController;
use App\Http\Controllers\Api\BureauDordreBDMController;
use App\Http\Controllers\Api\SociauCulturelBDMController;
use App\Http\Controllers\Api\AprovisionmentEtLogistiqueBDMController;
use App\Http\Controllers\Api\CentreInformatiqueBDMController;
use App\Http\Controllers\Api\DecarmetBDMController;
use App\Http\Controllers\Api\RessourcesHumainBDMController;
use App\Http\Controllers\Api\ServiceScolariteBDMController;
use App\Http\Controllers\Api\SecretariatGeneralBDMController;
use App\Http\Controllers\Api\LeDoyenBDMController;
use App\Http\Controllers\Api\VisChargerAfairesPedagogicBDMController;
use App\Http\Controllers\Api\VisChargerRechercheScientificBDMController;
use App\Http\Controllers\Api\FondanDePouvoirBDMController;
use App\Http\Controllers\Api\SaleDeTerageBDMController;
use App\Http\Controllers\Api\CentreDanalyseBDMController;
use App\Http\Controllers\Api\LogeEtudiantBDMController;
use App\Http\Controllers\Api\LogeAdministrationBDMController;
use App\Http\Controllers\Api\CentreDeConformanceBDMController;
use App\Http\Controllers\Api\BlocCBDMController;
use App\Http\Controllers\Api\KhanouchiBDMController;
use App\Http\Controllers\Api\MhamdiBDMController;
use App\Http\Controllers\Api\AchrafBDMController;
use App\Http\Controllers\Api\JaroudiBDMController;
use App\Http\Controllers\Api\OubrahimBDMController;
use App\Http\Controllers\Api\A1BDMController;
use App\Http\Controllers\Api\A2BDMController;
use App\Http\Controllers\Api\A3BDMController;
use App\Http\Controllers\Api\A4BDMController;
use App\Http\Controllers\Api\A5BDMController;
use App\Http\Controllers\Api\A6BDMController;
use App\Http\Controllers\Api\ChimieEtEnvirementBDMController;
use App\Http\Controllers\Api\ScienceDeVieBDMController;
use App\Http\Controllers\Api\LesLanguesBDMController;
use App\Http\Controllers\Api\MathematiquesBDMController;
use App\Http\Controllers\Api\GenieElectricBDMController;
use App\Http\Controllers\Api\GenieMecaniqueBDMController;
use App\Http\Controllers\Api\PhisiquesBDMController;
use App\Http\Controllers\Api\InformatiqueBDMController;
use App\Http\Controllers\Api\ScienceDeTerreBDMController;
use App\Http\Controllers\Api\FormationContinueBDMController;

Route::middleware('auth:sanctum')->get('/userBDM', function (Request $request) {
    return $request->user();
});    
Route::middleware(['cors'])->group(function () {
        Route::get('/achrafBDM', [AchrafBDMController::class, 'index']);
        Route::get('achrafBDM/{id}', [AchrafBDMController::class, 'show']);
        Route::post('achrafBDM', [AchrafBDMController::class, 'store']);
        Route::put('achrafBDM/{id}', [AchrafBDMController::class, 'update']);
        Route::delete('achraf/{id}/deleteBDM', [DecaAchrafControllerrmetBDMController::class, 'destroy']);

        Route::get('/bibleautequeBDM', [BibleautequeBDMController::class, 'index']);
        Route::get('/bibleautequeBDM/{id}', [BibleautequeBDMController::class, 'show']);
        Route::post('/bibleautequeBDM', [BibleautequeBDMController::class, 'store']);
        Route::put('/bibleautequeBDM/{id}', [BibleautequeBDMController::class, 'update']);
        Route::delete('/bibleauteque/{id}/deleteBDM', [BibleautequeBDMController::class, 'destroy']);
    
        Route::get('BureauDordreBDM', [BureauDordreBDMController::class, 'index']);
        Route::get('BureauDordreBDM/{id}', [BureauDordreBDMController::class, 'show']);
        Route::post('BureauDordreBDM', [BureauDordreBDMController::class, 'store']);
        Route::put('BureauDordreBDM/{id}', [BureauDordreBDMController::class, 'update']);
        Route::delete('BureauDordre/{id}/deleteBDM', [BureauDordreBDMController::class, 'destroy']);
    
        Route::get('SociauCulturelBDM', [SociauCulturelBDMController::class, 'index']);
        Route::get('SociauCulturelBDM/{id}', [SociauCulturelBDMController::class, 'show']);
        Route::post('SociauCulturelBDM', [SociauCulturelBDMController::class, 'store']);
        Route::put('SociauCulturelBDM/{id}', [SociauCulturelBDMController::class, 'update']);
        Route::delete('SociauCulturel/{id}/deleteBDM', [SociauCulturelBDMController::class, 'destroy']);
    
        Route::get('AprovisionmentEtLogistiqueBDM', [AprovisionmentEtLogistiqueBDMController::class, 'index']);
        Route::get('AprovisionmentEtLogistiqueBDM/{id}', [AprovisionmentEtLogistiqueBDMController::class, 'show']);
        Route::post('AprovisionmentEtLogistiqueBDM', [AprovisionmentEtLogistiqueBDMController::class, 'store']);
        Route::put('AprovisionmentEtLogistiqueBDM/{id}', [AprovisionmentEtLogistiqueBDMController::class, 'update']);
        Route::delete('AprovisionmentEtLogistique/{id}/deleteBDM', [AprovisionmentEtLogistiqueBDMController::class, 'destroy']);
    
        Route::get('CentreInformatiqueBDM', [CentreInformatiqueBDMController::class, 'index']);
        Route::get('CentreInformatiqueBDM/{id}', [CentreInformatiqueBDMController::class, 'show']);
        Route::post('CentreInformatiqueBDM', [CentreInformatiqueBDMController::class, 'store']);
        Route::put('CentreInformatiqueBDM/{id}', [CentreInformatiqueBDMController::class, 'update']);
        Route::delete('CentreInformatique/{id}/deleteBDM', [CentreInformatiqueBDMController::class, 'destroy']);
    
        Route::get('decarmetBDM', [DecarmetBDMController::class, 'index']);
        Route::get('decarmetBDM/{id}', [DecarmetBDMController::class, 'show']);
        Route::post('decarmetBDM', [DecarmetBDMController::class, 'store']);
        Route::put('decarmetBDM/{id}', [DecarmetBDMController::class, 'update']);
        Route::delete('decarmet/{id}/deleteBDM', [DecarmetBDMController::class, 'destroy']);

        Route::get('/ressourceshumainBDM', [RessourcesHumainBDMController::class, 'index']);
        Route::get('ressourceshumainBDM/{id}', [ressourceshumainBDMController::class, 'show']);
        Route::post('ressourceshumainBDM', [ressourceshumainBDMController::class, 'store']);
        Route::put('ressourceshumainBDM/{id}', [ressourceshumainBDMController::class, 'update']);
        Route::delete('ressourceshumain/{id}/deleteBDM', [ressourceshumainBDMController::class, 'destroy']);

        Route::get('/servicescolariteBDM', [ServiceScolariteBDMController::class, 'index']);
        Route::get('servicescolariteBDM/{id}', [servicescolariteBDMController::class, 'show']);
        Route::post('servicescolariteBDM', [servicescolariteBDMController::class, 'store']);
        Route::put('servicescolariteBDM/{id}', [servicescolariteBDMController::class, 'update']);
        Route::delete('servicescolarite/{id}/deleteBDM', [servicescolariteBDMController::class, 'destroy']);

        Route::get('/secretariatgeneralBDM', [SecretariatGeneralBDMController::class, 'index']);
        Route::get('secretariatgeneralBDM/{id}', [SecretariatGeneralBDMController::class, 'show']);
        Route::post('secretariatgeneralBDM', [SecretariatGeneralBDMController::class, 'store']);
        Route::put('secretariatgeneralBDM/{id}', [SecretariatGeneralBDMController::class, 'update']);
        Route::delete('secretariatgeneral/{id}/deleteBDM', [SecretariatGeneralBDMController::class, 'destroy']);

        Route::get('/ledoyenBDM', [LeDoyenBDMController::class, 'index']);
        Route::get('ledoyenBDM/{id}', [LeDoyenBDMController::class, 'show']);
        Route::post('ledoyenBDM', [LeDoyenBDMController::class, 'store']);
        Route::put('ledoyenBDM/{id}', [LeDoyenBDMController::class, 'update']);
        Route::delete('ledoyen/{id}/deleteBDM', [LeDoyenBDMController::class, 'destroy']);

        Route::get('/vischargerafairespedagogicBDM', [VisChargerAfairesPedagogicBDMController::class, 'index']);
        Route::get('vischargerafairespedagogicBDM/{id}', [VisChargerAfairesPedagogicBDMController::class, 'show']);
        Route::post('vischargerafairespedagogicBDM', [VisChargerAfairesPedagogicBDMController::class, 'store']);
        Route::put('vischargerafairespedagogicBDM/{id}', [VisChargerAfairesPedagogicBDMController::class, 'update']);
        Route::delete('vischargerafairespedagogic/{id}/deleteBDM', [VisChargerAfairesPedagogicBDMController::class, 'destroy']);

        Route::get('/vischargerrecherchescientificBDM', [VisChargerRechercheScientificBDMController::class, 'index']);
        Route::get('vischargerrecherchescientificBDM/{id}', [VisChargerRechercheScientificBDMController::class, 'show']);
        Route::post('vischargerrecherchescientificBDM', [VisChargerRechercheScientificBDMController::class, 'store']);
        Route::put('vischargerrecherchescientificBDM/{id}', [VisChargerRechercheScientificBDMController::class, 'update']);
        Route::delete('vischargerrecherchescientific/{id/delete}BDM', [VisChargerRechercheScientificBDMController::class, 'destroy']);

        Route::get('/fondandepouvoirBDM', [FondanDePouvoirBDMController::class, 'index']);
        Route::get('fondandepouvoirBDM/{id}', [FondanDePouvoirBDMController::class, 'show']);
        Route::post('fondandepouvoirBDM', [FondanDePouvoirBDMController::class, 'store']);
        Route::put('fondandepouvoirBDM/{id}', [FondanDePouvoirBDMController::class, 'update']);
        Route::delete('fondandepouvoir/{id}/deleteBDM', [FondanDePouvoirBDMController::class, 'destroy']);

        Route::get('/saledeterageBDM', [SaleDeTerageBDMController::class, 'index']);
        Route::get('saledeterageBDM/{id}', [SaleDeTerageBDMController::class, 'show']);
        Route::post('saledeterageBDM', [SaleDeTerageBDMController::class, 'store']);
        Route::put('saledeterageBDM/{id}', [SaleDeTerageBDMController::class, 'update']);
        Route::delete('saledeterage/{id}/deleteBDM', [SaleDeTerageBDMController::class, 'destroy']);

        Route::get('/centredanalyseBDM', [CentreDanalyseBDMController::class, 'index']);
        Route::get('centredanalyseBDM/{id}', [CentreDanalyseBDMController::class, 'show']);
        Route::post('centredanalyseBDM', [CentreDanalyseBDMController::class, 'store']);
        Route::put('centredanalyseBDM/{id}', [CentreDanalyseBDMController::class, 'update']);
        Route::delete('centredanalyse/{id}/deleteBDM', [CentreDanalyseBDMController::class, 'destroy']);

        Route::get('decarmetBDM/{id}', [DecarmetBDMController::class, 'show']);
        Route::post('decarmetBDM', [DecarmetBDMController::class, 'store']);
        Route::put('decarmetBDM/{id}', [DecarmetBDMController::class, 'update']);
        Route::delete('decarmet/{id}/deleteBDM', [DecarmetBDMController::class, 'destroy']);

        Route::get('/logeetudiantBDM', [LogeEtudiantBDMController::class, 'index']);
        Route::get('logeetudiantBDM/{id}', [LogeEtudiantBDMController::class, 'show']);
        Route::post('logeetudiantBDM', [LogeEtudiantBDMController::class, 'store']);
        Route::put('logeetudiantBDM/{id}', [LogeEtudiantBDMController::class, 'update']);
        Route::delete('logeetudiant/{id}/deleteBDM', [LogeEtudiantBDMController::class, 'destroy']);

        Route::get('/logeadministrationBDM', [LogeAdministrationBDMController::class, 'index']);
        Route::get('logeadministrationBDM/{id}', [LogeAdministrationBDMController::class, 'show']);
        Route::post('logeadministrationBDM', [LogeAdministrationBDMController::class, 'store']);
        Route::put('logeadministrationBDM/{id}', [LogeAdministrationBDMController::class, 'update']);
        Route::delete('logeadministration/{id}/deleteBDM', [LogeAdministrationBDMController::class, 'destroy']);

        Route::get('/centredeconformanceBDM', [CentreDeConformanceBDMController::class, 'index']);
        Route::get('centredeconformanceBDM/{id}', [CentreDeConformanceBDMController::class, 'show']);
        Route::post('centredeconformanceBDM', [CentreDeConformanceBDMController::class, 'store']);
        Route::put('centredeconformanceBDM/{id}', [CentreDeConformanceBDMController::class, 'update']);
        Route::delete('centredeconformance/{id}/deleteBDM', [CentreDeConformanceBDMController::class, 'destroy']);

        Route::get('/bloccBDM', [BlocCBDMController::class, 'index']);
        Route::get('bloccBDM/{id}', [BlocCBDMController::class, 'show']);
        Route::post('bloccBDM', [BlocCBDMController::class, 'store']);
        Route::put('bloccBDM/{id}', [BlocCBDMController::class, 'update']);
        Route::delete('blocc/{id}/deleteBDM', [BlocCBDMController::class, 'destroy']);


            Route::get('/bloccBDM', [KhanouchiBDMController::class, 'index']);
            Route::get('bloccBDM/{id}', [KhanouchiBDMController::class, 'show']);
            Route::post('bloccBDM', [KhanouchiBDMController::class, 'store']);
            Route::put('bloccBDM/{id}', [KhanouchiBDMController::class, 'update']);
            Route::delete('blocc/{id}/deleteBDM', [KhanouchiBDMController::class, 'destroy']);

            Route::get('/mhamdiBDM', [MhamdiBDMController::class, 'index']);
            Route::get('mhamdiBDM/{id}', [MhamdiBDMController::class, 'show']);
            Route::post('mhamdiBDM', [MhamdiBDMController::class, 'store']);
            Route::put('mhamdiBDM/{id}', [MhamdiBDMController::class, 'update']);
            Route::delete('mhamdi/{id}/deleteBDM', [MhamdiBDMController::class, 'destroy']);

           

            Route::get('/jaroudiBDM', [JaroudiBDMController::class, 'index']);
            Route::get('jaroudiBDM/{id}', [JaroudiBDMController::class, 'show']);
            Route::post('jaroudiBDM', [JaroudiBDMController::class, 'store']);
            Route::put('jaroudiBDM/{id}', [JaroudiBDMController::class, 'update']);
            Route::delete('jaroudi/{id}/deleteBDM', [JaroudiBDMController::class, 'destroy']);

            Route::get('/oubrahimBDM', [OubrahimBDMController::class, 'index']);
            Route::get('oubrahimBDM/{id}', [OubrahimBDMController::class, 'show']);
            Route::post('oubrahimBDM', [OubrahimBDMController::class, 'store']);
            Route::put('oubrahimBDM/{id}', [OubrahimBDMController::class, 'update']);
            Route::delete('oubrahim/{id}/deleteBDM', [OubrahimBDMController::class, 'destroy']);
    
            Route::get('/a1BDM', [A1BDMController::class, 'index']);
            Route::get('a1BDM/{id}', [A1BDMController::class, 'show']);
            Route::post('a1BDM', [A1BDMController::class, 'store']);
            Route::put('a1BDM/{id}', [A1BDMController::class, 'update']);
            Route::delete('a1/{id}/deleteBDM', [A1BDMController::class, 'destroy']);
    
            Route::get('/a2BDM', [A2BDMController::class, 'index']);
            Route::get('a2BDM/{id}', [A2BDMController::class, 'show']);
            Route::post('a2BDM', [A2BDMController::class, 'store']);
            Route::put('a2BDM/{id}', [A2BDMController::class, 'update']);
            Route::delete('a2/{id}/deleteBDM', [A2BDMController::class, 'destroy']);
    
            Route::get('/a3BDM', [A3BDMController::class, 'index']);
            Route::get('a3BDM/{id}', [A3BDMController::class, 'show']);
            Route::post('a3BDM', [A3BDMController::class, 'store']);
            Route::put('a3BDM/{id}', [A3BDMController::class, 'update']);
            Route::delete('a3/{id}/deleteBDM', [A3BDMController::class, 'destroy']);
    
            Route::get('/a4BDM', [A4BDMController::class, 'index']);
            Route::get('a4BDM/{id}', [A4BDMController::class, 'show']);
            Route::post('a4BDM', [A4BDMController::class, 'store']);
            Route::put('a4BDM/{id}', [A4BDMController::class, 'update']);
            Route::delete('a4/{id}/deleteBDM', [A4BDMController::class, 'destroy']);
    
            Route::get('/a5BDM', [A5BDMController::class, 'index']);
            Route::get('a5BDM/{id}', [A5BDMController::class, 'show']);
            Route::post('a5BDM', [A5BDMController::class, 'store']);
            Route::put('a5BDM/{id}', [A5BDMController::class, 'update']);
            Route::delete('a5/{id}/deleteBDM', [A5BDMController::class, 'destroy']);
    
            Route::get('/a6BDM', [A6BDMController::class, 'index']);
            Route::get('a6BDM/{id}', [A6BDMController::class, 'show']);
            Route::post('a6BDM', [A6BDMController::class, 'store']);
            Route::put('a6BDM/{id}', [A6BDMController::class, 'update']);
            Route::delete('a6/{id}/deleteBDM', [A6BDMController::class, 'destroy']);
    
    
    Route::get('/chimieetenvirementBDM', [ChimieEtEnvirementBDMController::class, 'index']);
    Route::get('chimieetenvirementBDM/{id}', [ChimieEtEnvirementBDMController::class, 'show']);
            Route::post('chimieetenvirementBDM', [ChimieEtEnvirementBDMController::class, 'store']);
            Route::put('chimieetenvirementBDM/{id}', [ChimieEtEnvirementBDMController::class, 'update']);
            Route::delete('chimieetenvirement/{id}/deleteBDM', [ChimieEtEnvirementBDMController::class, 'destroy']);
    

    Route::get('/sciencedevieBDM', [ScienceDeVieBDMController::class, 'index']);
    Route::get('sciencedevieBDM/{id}', [ScienceDeVieBDMController::class, 'show']);
            Route::post('sciencedevieBDM', [ScienceDeVieBDMController::class, 'store']);
            Route::put('sciencedevieBDM/{id}', [ScienceDeVieBDMController::class, 'update']);
            Route::delete('sciencedevie/{id}/deleteBDM', [ScienceDeVieBDMController::class, 'destroy']);
    
    Route::get('/leslanguesBDM', [LesLanguesBDMController::class, 'index']);
    Route::get('leslanguesBDM/{id}', [LesLanguesBDMController::class, 'show']);
            Route::post('leslanguesBDM', [LesLanguesBDMController::class, 'store']);
            Route::put('leslanguesBDM/{id}', [LesLanguesBDMController::class, 'update']);
            Route::delete('leslangues/{id}/deleteBDM', [LesLanguesBDMController::class, 'destroy']);
    
    Route::get('/mathematiquesBDM', [MathematiquesBDMController::class, 'index']);
    Route::get('mathematiquesBDM/{id}', [MathematiquesBDMController::class, 'show']);
            Route::post('mathematiquesBDM', [MathematiquesBDMController::class, 'store']);
            Route::put('mathematiquesBDM/{id}', [MathematiquesBDMController::class, 'update']);
            Route::delete('mathematiques/{id}/deleteBDM', [MathematiquesBDMController::class, 'destroy']);
    
    Route::get('/genieelectricBDM', [GenieElectricBDMController::class, 'index']);
    Route::get('genieelectricBDM/{id}', [GenieElectricBDMController::class, 'show']);
            Route::post('genieelectricBDM', [GenieElectricBDMController::class, 'store']);
            Route::put('genieelectricBDM/{id}', [GenieElectricBDMController::class, 'update']);
            Route::delete('genieelectric/{id}/deleteBDM', [GenieElectricBDMController::class, 'destroy']);
    
    Route::get('/geniemecaniqueBDM', [GenieMecaniqueBDMController::class, 'index']);
    Route::get('geniemecaniqueBDM/{id}', [GenieMecaniqueBDMController::class, 'show']);
            Route::post('geniemecaniqueBDM', [GenieMecaniqueBDMController::class, 'store']);
            Route::put('geniemecaniqueBDM/{id}', [GenieMecaniqueBDMController::class, 'update']);
            Route::delete('geniemecanique/{id}/deleteBDM', [GenieMecaniqueBDMController::class, 'destroy']);
    
    Route::get('/phisiquesBDM', [PhisiquesBDMController::class, 'index']);
    Route::get('phisiquesBDM/{id}', [PhisiquesBDMController::class, 'show']);
            Route::post('phisiquesBDM', [PhisiquesBDMController::class, 'store']);
            Route::put('phisiquesBDM/{id}', [PhisiquesBDMController::class, 'update']);
            Route::delete('phisiques/{id}/deleteBDM', [PhisiquesBDMController::class, 'destroy']);
    
    Route::get('/informatiqueBDM', [InformatiqueBDMController::class, 'index']);
    Route::get('informatiqueBDM/{id}', [InformatiqueBDMController::class, 'show']);
            Route::post('informatiqueBDM', [InformatiqueBDMController::class, 'store']);
            Route::put('informatiqueBDM/{id}', [InformatiqueBDMController::class, 'update']);
            Route::delete('informatique/{id}/deleteBDM', [InformatiqueBDMController::class, 'destroy']);
    
    Route::get('/sciencedeterreBDM', [ScienceDeTerreBDMController::class, 'index']);
    Route::get('sciencedeterreBDM/{id}', [ScienceDeTerreBDMController::class, 'show']);
            Route::post('sciencedeterreBDM', [ScienceDeTerreBDMController::class, 'store']);
            Route::put('sciencedeterreBDM/{id}', [ScienceDeTerreBDMController::class, 'update']);
            Route::delete('sciencedeterre/{id/delete}BDM', [ScienceDeTerreBDMController::class, 'destroy']);
    
    Route::get('/formationcontinueBDM', [FormationContinueBDMController::class, 'index']);
    Route::get('formationcontinueBDM/{id}', [formationcontinueBDMController::class, 'show']);
            Route::post('formationcontinueBDM', [formationcontinueBDMController::class, 'store']);
            Route::put('formationcontinueBDM/{id}', [formationcontinueBDMController::class, 'update']);
            Route::delete('formationcontinue/{id}/deleteBDM', [formationcontinueBDMController::class, 'destroy']);
    
        });