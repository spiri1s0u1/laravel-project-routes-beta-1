<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('genie_mecanique_b_d_m', function (Blueprint $table) {
            $table->id();
            $table->integer('Ndordre');
            $table->text('Designation');
            $table->integer('Quantitie');
            $table->string('Observation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('genie_mecanique_b_d_m');
    }
};
