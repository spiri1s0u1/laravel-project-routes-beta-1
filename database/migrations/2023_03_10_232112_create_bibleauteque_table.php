<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bibleauteque', function (Blueprint $table) {
            $table->id();
            $table->string('fourniteurLeBureau')->default('');
            $table->string('besoinInformatique')->default('');
            $table->string('lentetient')->default('');
            $table->string('autres')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bibleauteque');
    }
};
