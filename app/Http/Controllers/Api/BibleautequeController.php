<?php

namespace App\Http\Controllers\Api;

use App\Models\bibleauteque;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class bibleautequeController extends Controller
{
    public function index()
    {
        $bibleauteques = bibleauteque::all();

        if($bibleauteques->count()>0){
            return response()->json([
                'status'=>200,
                "bibleauteques"=>$bibleauteques
            ],200);
        }
        return response()->json([
            'status'=>404,
            "bibleauteques"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "bibleauteques"=>$validator->messages()
                ],422);   
            }else{
                $bibleauteque = bibleauteque::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($bibleauteque){
                return response()->json([
                    'status'=>200,
                    "bibleauteques"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "bibleauteques"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $bibleauteque = bibleauteque::find($id);
        if($bibleauteque){
            return response()->json([
                'status'=>200,
                "bibleauteques"=>$bibleauteque
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "bibleauteques"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, bibleauteque $bibleauteque)
    {
        $bibleauteque->update($request->all());
        return response()->json($bibleauteque);
    }

    public function destroy($id)
    {
        $bibleauteque = bibleauteque::find($id);
        if($bibleauteque){

            $bibleauteque->delete();
        }else{
            return response()->json([
                'status'=>404,
                "bibleauteques"=>"not found"
            ],404);
    }
}
}