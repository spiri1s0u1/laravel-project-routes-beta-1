<?php

namespace App\Http\Controllers\Api;

use App\Models\GenieElectric;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class GenieElectricController extends Controller
{
    public function index()
    {
        $GenieElectrics = GenieElectric::all();

        if($GenieElectrics->count()>0){
            return response()->json([
                'status'=>200,
                "GenieElectrics"=>$GenieElectrics
            ],200);
        }
        return response()->json([
            'status'=>404,
            "GenieElectrics"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "GenieElectrics"=>$validator->messages()
                ],422);   
            }else{
                $GenieElectric = GenieElectric::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($GenieElectric){
                return response()->json([
                    'status'=>200,
                    "GenieElectrics"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "GenieElectrics"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $GenieElectric = GenieElectric::find($id);
        if($GenieElectric){
            return response()->json([
                'status'=>200,
                "GenieElectrics"=>$GenieElectric
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "GenieElectrics"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, GenieElectric $GenieElectric)
    {
        $GenieElectric->update($request->all());
        return response()->json($GenieElectric);
    }

    public function destroy(GenieElectric $GenieElectric)
    {
        $GenieElectric->delete();
        return response()->json('GenieElectric deleted successfully');
    }
}
