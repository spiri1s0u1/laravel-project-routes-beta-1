<?php

namespace App\Http\Controllers\Api;

use App\Models\CentreDeConformanceBDM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class CentreDeConformanceBDMController extends Controller
{
    public function index()
    {
        $CentreDeConformanceBDMs = CentreDeConformanceBDM::all();

        if($CentreDeConformanceBDMs->count()>0){
            return response()->json([
                'status'=>200,
                "CentreDeConformanceBDMs"=>$CentreDeConformanceBDMs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "CentreDeConformanceBDMs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "CentreDeConformanceBDMs"=>$validator->messages()
                ],422);   
            }else{
                $CentreDeConformanceBDM = CentreDeConformanceBDM::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($CentreDeConformanceBDM){
                return response()->json([
                    'status'=>200,
                    "CentreDeConformanceBDMs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "CentreDeConformanceBDMs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $CentreDeConformanceBDM = CentreDeConformanceBDM::find($id);
        if($CentreDeConformanceBDM){
            return response()->json([
                'status'=>200,
                "CentreDeConformanceBDMs"=>$CentreDeConformanceBDM
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "CentreDeConformanceBDMs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, CentreDeConformanceBDM $CentreDeConformanceBDM)
    {
        $CentreDeConformanceBDM->update($request->all());
        return response()->json($CentreDeConformanceBDM);
    }

    public function destroy(CentreDeConformanceBDM $CentreDeConformanceBDM)
    {
        $CentreDeConformanceBDM->delete();
        return response()->json('CentreDeConformanceBDM deleted successfully');
    }
}

