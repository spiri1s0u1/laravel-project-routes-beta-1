<?php

namespace App\Http\Controllers\Api;

use App\Models\CentreDanalyseBDM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class CentreDanalyseBDMController extends Controller
{
    public function index()
    {
        $CentreDanalyseBDMs = CentreDanalyseBDM::all();

        if($CentreDanalyseBDMs->count()>0){
            return response()->json([
                'status'=>200,
                "CentreDanalyseBDMs"=>$CentreDanalyseBDMs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "CentreDanalyseBDMs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "CentreDanalyseBDMs"=>$validator->messages()
                ],422);   
            }else{
                $CentreDanalyseBDM = CentreDanalyseBDM::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($CentreDanalyseBDM){
                return response()->json([
                    'status'=>200,
                    "CentreDanalyseBDMs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "CentreDanalyseBDMs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $CentreDanalyseBDM = CentreDanalyseBDM::find($id);
        if($CentreDanalyseBDM){
            return response()->json([
                'status'=>200,
                "CentreDanalyseBDMs"=>$CentreDanalyseBDM
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "CentreDanalyseBDMs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, CentreDanalyseBDM $CentreDanalyseBDM)
    {
        $CentreDanalyseBDM->update($request->all());
        return response()->json($CentreDanalyseBDM);
    }

    public function destroy(CentreDanalyseBDM $CentreDanalyseBDM)
    {
        $CentreDanalyseBDM->delete();
        return response()->json('CentreDanalyseBDM deleted successfully');
    }
}

