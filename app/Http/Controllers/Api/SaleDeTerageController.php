<?php

namespace App\Http\Controllers\Api;

use App\Models\SaleDeTerage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class A6Controller extends Controller
{
    public function index()
    {
        $A6s = SaleDeTerage::all();

        if($A6s->count()>0){
            return response()->json([
                'status'=>200,
                "A6s"=>$A6s
            ],200);
        }
        return response()->json([
            'status'=>404,
            "A6s"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "A6s"=>$validator->messages()
                ],422);   
            }else{
                $SaleDeTerage = SaleDeTerage::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($SaleDeTerage){
                return response()->json([
                    'status'=>200,
                    "A6s"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "A6s"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $SaleDeTerage = SaleDeTerage::find($id);
        if($SaleDeTerage){
            return response()->json([
                'status'=>200,
                "A6s"=>$SaleDeTerage
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "A6s"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, SaleDeTerage $SaleDeTerage)
    {
        $SaleDeTerage->update($request->all());
        return response()->json($SaleDeTerage);
    }

    public function destroy(SaleDeTerage $SaleDeTerage)
    {
        $SaleDeTerage->delete();
        return response()->json('SaleDeTerage deleted successfully');
    }
}


