<?php

namespace App\Http\Controllers\Api;

use App\Models\Decarmet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class DecarmetController extends Controller
{
    public function index()
    {
        $Decarmets = Decarmet::all();

        if($Decarmets->count()>0){
            return response()->json([
                'status'=>200,
                "Decarmets"=>$Decarmets
            ],200);
        }
        return response()->json([
            'status'=>404,
            "Decarmets"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "Decarmets"=>$validator->messages()
                ],422);   
            }else{
                $Decarmet = Decarmet::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($Decarmet){
                return response()->json([
                    'status'=>200,
                    "Decarmets"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "Decarmets"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $Decarmet = Decarmet::find($id);
        if($Decarmet){
            return response()->json([
                'status'=>200,
                "Decarmets"=>$Decarmet
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "Decarmets"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, Decarmet $Decarmet)
    {
        $Decarmet->update($request->all());
        return response()->json($Decarmet);
    }

    public function destroy(Decarmet $Decarmet)
    {
        $Decarmet->delete();
        return response()->json('Decarmet deleted successfully');
    }
}


