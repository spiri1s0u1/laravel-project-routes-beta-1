<?php

namespace App\Http\Controllers\Api;

use App\Models\BureauDordre;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class BureauDordreController extends Controller
{
    public function index()
    {
        $BureauDordres = BureauDordre::all();

        if($BureauDordres->count()>0){
            return response()->json([
                'status'=>200,
                "BureauDordres"=>$BureauDordres
            ],200);
        }
        return response()->json([
            'status'=>404,
            "BureauDordres"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fourniteurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "BureauDordres"=>$validator->messages()
                ],422);   
            }else{
                $BureauDordre = BureauDordre::create([
                    'fourniteurLeBureau' => $request->fourniteurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($BureauDordre){
                return response()->json([
                    'status'=>200,
                    "BureauDordres"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "BureauDordres"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $BureauDordre = BureauDordre::find($id);
        if($BureauDordre){
            return response()->json([
                'status'=>200,
                "BureauDordres"=>$BureauDordre
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "BureauDordres"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, BureauDordre $BureauDordre)
    {
        $BureauDordre->update($request->all());
        return response()->json($BureauDordre);
    }

    public function destroy(BureauDordre $BureauDordre)
    {
        $BureauDordre->delete();
        return response()->json('BureauDordre deleted successfully');
    }
}
