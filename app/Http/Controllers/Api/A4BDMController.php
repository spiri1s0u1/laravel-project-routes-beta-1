<?php

namespace App\Http\Controllers\Api;

use App\Models\a4BDM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class a4BDMController extends Controller
{
    public function index()
    {
        $a4BDMs = a4BDM::all();

        if($a4BDMs->count()>0){
            return response()->json([
                'status'=>200,
                "a4BDMs"=>$a4BDMs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "a4BDMs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "a4BDMs"=>$validator->messages()
                ],422);   
            }else{
                $a4BDM = a4BDM::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($a4BDM){
                return response()->json([
                    'status'=>200,
                    "a4BDMs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "a4BDMs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $a4BDM = a4BDM::find($id);
        if($a4BDM){
            return response()->json([
                'status'=>200,
                "a4BDMs"=>$a4BDM
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "a4BDMs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, a4BDM $a4BDM)
    {
        $a4BDM->update($request->all());
        return response()->json($a4BDM);
    }

    public function destroy(a4BDM $a4BDM)
    {
        $a4BDM->delete();
        return response()->json('a4BDM deleted successfully');
    }
}
