<?php

namespace App\Http\Controllers\Api;

use App\Models\Informatique;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class InformatiqueController extends Controller
{
    public function index()
    {
        $Informatiques = Informatique::all();

        if($Informatiques->count()>0){
            return response()->json([
                'status'=>200,
                "Informatiques"=>$Informatiques
            ],200);
        }
        return response()->json([
            'status'=>404,
            "Informatiques"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "Informatiques"=>$validator->messages()
                ],422);   
            }else{
                $Informatique = Informatique::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($Informatique){
                return response()->json([
                    'status'=>200,
                    "Informatiques"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "Informatiques"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $Informatique = Informatique::find($id);
        if($Informatique){
            return response()->json([
                'status'=>200,
                "Informatiques"=>$Informatique
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "Informatiques"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, Informatique $Informatique)
    {
        $Informatique->update($request->all());
        return response()->json($Informatique);
    }

    public function destroy(Informatique $Informatique)
    {
        $Informatique->delete();
        return response()->json('Informatique deleted successfully');
    }
}
