<?php

namespace App\Http\Controllers\Api;

use App\Models\BureauDordreBDM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class BureauDordreBDMController extends Controller
{
    public function index()
    {
        $BureauDordreBDMs = BureauDordreBDM::all();

        if($BureauDordreBDMs->count()>0){
            return response()->json([
                'status'=>200,
                "BureauDordreBDMs"=>$BureauDordreBDMs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "BureauDordreBDMs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'Ndordre'=> 'required|integer',
                'Designation'=> 'required|string|max:190',
                'Quantitie'=> 'required|integer',
                'Observation'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "BureauDordreBDMs"=>$validator->messages()
                ],422);   
            }else{
                $BureauDordreBDM = BureauDordreBDM::create([
                    'Ndordre' => $request->Ndordre,
                    'Designation' => $request->Designation,
                    'Quantitie' => $request->Quantitie,   
                    'Observation' => $request->Quantitie,

                ]);
            }if($BureauDordreBDM){
                return response()->json([
                    'status'=>200,
                    "BureauDordreBDMs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "BureauDordreBDMs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $BureauDordreBDM = BureauDordreBDM::find($id);
        if($BureauDordreBDM){
            return response()->json([
                'status'=>200,
                "BureauDordreBDMs"=>$BureauDordreBDM
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "BureauDordreBDMs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, BureauDordreBDM $BureauDordreBDM)
    {
        $BureauDordreBDM->update($request->all());
        return response()->json($BureauDordreBDM);
    }

    public function destroy(BureauDordreBDM $BureauDordreBDM)
    {
        $BureauDordreBDM->delete();
        return response()->json('BureauDordreBDM deleted successfully');
    }
}
