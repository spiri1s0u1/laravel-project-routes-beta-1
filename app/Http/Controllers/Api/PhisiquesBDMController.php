<?php

namespace App\Http\Controllers\Api;

use App\Models\Phisiques;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class PhisiquesController extends Controller
{
    public function index()
    {
        $Phisiquess = Phisiques::all();

        if($Phisiquess->count()>0){
            return response()->json([
                'status'=>200,
                "Phisiquess"=>$Phisiquess
            ],200);
        }
        return response()->json([
            'status'=>404,
            "Phisiquess"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "Phisiquess"=>$validator->messages()
                ],422);   
            }else{
                $Phisiques = Phisiques::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($Phisiques){
                return response()->json([
                    'status'=>200,
                    "Phisiquess"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "Phisiquess"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $Phisiques = Phisiques::find($id);
        if($Phisiques){
            return response()->json([
                'status'=>200,
                "Phisiquess"=>$Phisiques
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "Phisiquess"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, Phisiques $Phisiques)
    {
        $Phisiques->update($request->all());
        return response()->json($Phisiques);
    }

    public function destroy(Phisiques $Phisiques)
    {
        $Phisiques->delete();
        return response()->json('Phisiques deleted successfully');
    }
}

