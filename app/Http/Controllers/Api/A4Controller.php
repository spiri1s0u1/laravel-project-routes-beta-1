<?php

namespace App\Http\Controllers\Api;

use App\Models\a4;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class a4Controller extends Controller
{
    public function index()
    {
        $a4s = a4::all();

        if($a4s->count()>0){
            return response()->json([
                'status'=>200,
                "a4s"=>$a4s
            ],200);
        }
        return response()->json([
            'status'=>404,
            "a4s"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "a4s"=>$validator->messages()
                ],422);   
            }else{
                $a4 = a4::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($a4){
                return response()->json([
                    'status'=>200,
                    "a4s"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "a4s"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $a4 = a4::find($id);
        if($a4){
            return response()->json([
                'status'=>200,
                "a4s"=>$a4
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "a4s"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, a4 $a4)
    {
        $a4->update($request->all());
        return response()->json($a4);
    }

    public function destroy(a4 $a4)
    {
        $a4->delete();
        return response()->json('a4 deleted successfully');
    }
}
