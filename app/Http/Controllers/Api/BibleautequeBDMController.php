<?php

namespace App\Http\Controllers\Api;

use App\Models\bibleautequeBDM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class bibleautequeBDMController extends Controller
{
    public function index()
    {
        $bibleautequeBDMs = bibleautequeBDM::all();

        if($bibleautequeBDMs->count()>0){
            return response()->json([
                'status'=>200,
                "bibleautequeBDMs"=>$bibleautequeBDMs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "bibleautequeBDMs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'Ndordre'=> 'required|integer',
                'Designation'=> 'required|string|max:190',
                'Quantitie'=> 'required|integer',
                'Observation'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "bibleautequeBDMs"=>$validator->messages()
                ],422);   
            }else{
                $bibleautequeBDM = bibleautequeBDM::create([
                    'Ndordre' => $request->Ndordre,
                    'Designation' => $request->Designation,
                    'Quantitie' => $request->Quantitie,   
                    'Observation' => $request->Quantitie,

                ]);
            }if($bibleautequeBDM){
                return response()->json([
                    'status'=>200,
                    "bibleautequeBDMs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "bibleautequeBDMs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $bibleautequeBDM = bibleautequeBDM::find($id);
        if($bibleautequeBDM){
            return response()->json([
                'status'=>200,
                "bibleautequeBDMs"=>$bibleautequeBDM
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "bibleautequeBDMs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, bibleautequeBDM $bibleautequeBDM)
    {
        $bibleautequeBDM->update($request->all());
        return response()->json($bibleautequeBDM);
    }

    public function destroy($id)
    {
        $bibleautequeBDM = bibleautequeBDM::find($id);
        if($bibleautequeBDM){

            $bibleautequeBDM->delete();
        }else{
            return response()->json([
                'status'=>404,
                "bibleautequeBDMs"=>"not found"
            ],404);
    }
}
}