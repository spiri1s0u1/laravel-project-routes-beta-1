<?php

namespace App\Http\Controllers\Api;

use App\Models\ScienceDeTerre;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class ScienceDeTerreController extends Controller
{
    public function index()
    {
        $ScienceDeTerres = ScienceDeTerre::all();

        if($ScienceDeTerres->count()>0){
            return response()->json([
                'status'=>200,
                "ScienceDeTerres"=>$ScienceDeTerres
            ],200);
        }
        return response()->json([
            'status'=>404,
            "ScienceDeTerres"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "ScienceDeTerres"=>$validator->messages()
                ],422);   
            }else{
                $ScienceDeTerre = ScienceDeTerre::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($ScienceDeTerre){
                return response()->json([
                    'status'=>200,
                    "ScienceDeTerres"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "ScienceDeTerres"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $ScienceDeTerre = ScienceDeTerre::find($id);
        if($ScienceDeTerre){
            return response()->json([
                'status'=>200,
                "ScienceDeTerres"=>$ScienceDeTerre
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "ScienceDeTerres"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, ScienceDeTerre $ScienceDeTerre)
    {
        $ScienceDeTerre->update($request->all());
        return response()->json($ScienceDeTerre);
    }

    public function destroy(ScienceDeTerre $ScienceDeTerre)
    {
        $ScienceDeTerre->delete();
        return response()->json('ScienceDeTerre deleted successfully');
    }
}

