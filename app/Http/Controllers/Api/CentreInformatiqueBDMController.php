<?php

namespace App\Http\Controllers\Api;

use App\Models\CentreInformatiqueBDM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class CentreInformatiqueBDMController extends Controller
{
    public function index()
    {
        $CentreInformatiqueBDMs = CentreInformatiqueBDM::all();

        if($CentreInformatiqueBDMs->count()>0){
            return response()->json([
                'status'=>200,
                "CentreInformatiqueBDMs"=>$CentreInformatiqueBDMs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "CentreInformatiqueBDMs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "CentreInformatiqueBDMs"=>$validator->messages()
                ],422);   
            }else{
                $CentreInformatiqueBDM = CentreInformatiqueBDM::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($CentreInformatiqueBDM){
                return response()->json([
                    'status'=>200,
                    "CentreInformatiqueBDMs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "CentreInformatiqueBDMs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $CentreInformatiqueBDM = CentreInformatiqueBDM::find($id);
        if($CentreInformatiqueBDM){
            return response()->json([
                'status'=>200,
                "CentreInformatiqueBDMs"=>$CentreInformatiqueBDM
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "CentreInformatiqueBDMs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, CentreInformatiqueBDM $CentreInformatiqueBDM)
    {
        $CentreInformatiqueBDM->update($request->all());
        return response()->json($CentreInformatiqueBDM);
    }

    public function destroy(CentreInformatiqueBDM $CentreInformatiqueBDM)
    {
        $CentreInformatiqueBDM->delete();
        return response()->json('CentreInformatiqueBDM deleted successfully');
    }
}
