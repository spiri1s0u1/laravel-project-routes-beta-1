<?php

namespace App\Http\Controllers\Api;

use App\Models\CentreDanalyse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class CentreDanalyseController extends Controller
{
    public function index()
    {
        $CentreDanalyses = CentreDanalyse::all();

        if($CentreDanalyses->count()>0){
            return response()->json([
                'status'=>200,
                "CentreDanalyses"=>$CentreDanalyses
            ],200);
        }
        return response()->json([
            'status'=>404,
            "CentreDanalyses"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "CentreDanalyses"=>$validator->messages()
                ],422);   
            }else{
                $CentreDanalyse = CentreDanalyse::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($CentreDanalyse){
                return response()->json([
                    'status'=>200,
                    "CentreDanalyses"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "CentreDanalyses"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $CentreDanalyse = CentreDanalyse::find($id);
        if($CentreDanalyse){
            return response()->json([
                'status'=>200,
                "CentreDanalyses"=>$CentreDanalyse
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "CentreDanalyses"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, CentreDanalyse $CentreDanalyse)
    {
        $CentreDanalyse->update($request->all());
        return response()->json($CentreDanalyse);
    }

    public function destroy(CentreDanalyse $CentreDanalyse)
    {
        $CentreDanalyse->delete();
        return response()->json('CentreDanalyse deleted successfully');
    }
}

