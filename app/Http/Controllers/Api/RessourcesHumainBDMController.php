<?php

namespace App\Http\Controllers\Api;

use App\Models\RessourcesHumain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class RessourcesHumainController extends Controller
{
    public function index()
    {
        $RessourcesHumains = RessourcesHumain::all();

        if($RessourcesHumains->count()>0){
            return response()->json([
                'status'=>200,
                "RessourcesHumains"=>$RessourcesHumains
            ],200);
        }
        return response()->json([
            'status'=>404,
            "RessourcesHumains"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "RessourcesHumains"=>$validator->messages()
                ],422);   
            }else{
                $RessourcesHumain = RessourcesHumain::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($RessourcesHumain){
                return response()->json([
                    'status'=>200,
                    "RessourcesHumains"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "RessourcesHumains"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $RessourcesHumain = RessourcesHumain::find($id);
        if($RessourcesHumain){
            return response()->json([
                'status'=>200,
                "RessourcesHumains"=>$RessourcesHumain
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "RessourcesHumains"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, RessourcesHumain $RessourcesHumain)
    {
        $RessourcesHumain->update($request->all());
        return response()->json($RessourcesHumain);
    }

    public function destroy(RessourcesHumain $RessourcesHumain)
    {
        $RessourcesHumain->delete();
        return response()->json('RessourcesHumain deleted successfully');
    }
}
