<?php

namespace App\Http\Controllers\Api;

use App\Models\LogeAdministration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class LogeAdministrationController extends Controller
{
    public function index()
    {
        $LogeAdministrations = LogeAdministration::all();

        if($LogeAdministrations->count()>0){
            return response()->json([
                'status'=>200,
                "LogeAdministrations"=>$LogeAdministrations
            ],200);
        }
        return response()->json([
            'status'=>404,
            "LogeAdministrations"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "LogeAdministrations"=>$validator->messages()
                ],422);   
            }else{
                $LogeAdministration = LogeAdministration::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($LogeAdministration){
                return response()->json([
                    'status'=>200,
                    "LogeAdministrations"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "LogeAdministrations"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $LogeAdministration = LogeAdministration::find($id);
        if($LogeAdministration){
            return response()->json([
                'status'=>200,
                "LogeAdministrations"=>$LogeAdministration
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "LogeAdministrations"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, LogeAdministration $LogeAdministration)
    {
        $LogeAdministration->update($request->all());
        return response()->json($LogeAdministration);
    }

    public function destroy(LogeAdministration $LogeAdministration)
    {
        $LogeAdministration->delete();
        return response()->json('LogeAdministration deleted successfully');
    }
}
