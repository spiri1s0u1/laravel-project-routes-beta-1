<?php

namespace App\Http\Controllers\Api;

use App\Models\a2;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class a2Controller extends Controller
{
    public function index()
    {
        $a2s = a2::all();

        if($a2s->count()>0){
            return response()->json([
                'status'=>200,
                "a2s"=>$a2s
            ],200);
        }
        return response()->json([
            'status'=>404,
            "a2s"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:290',
                'besoinInformatique'=> 'required|string|max:290',
                'lentetient'=> 'required|string|max:290',
                'autres'=> 'required|string|max:290',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "a2s"=>$validator->messages()
                ],422);   
            }else{
                $a2 = a2::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($a2){
                return response()->json([
                    'status'=>200,
                    "a2s"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "a2s"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $a2 = a2::find($id);
        if($a2){
            return response()->json([
                'status'=>200,
                "a2s"=>$a2
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "a2s"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, a2 $a2)
    {
        $a2->update($request->all());
        return response()->json($a2);
    }

    public function destroy(a2 $a2)
    {
        $a2->delete();
        return response()->json('a2 deleted successfully');
    }
}
