<?php

namespace App\Http\Controllers\Api;

use App\Models\A6;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class A6Controller extends Controller
{
    public function index()
    {
        $A6s = A6::all();

        if($A6s->count()>0){
            return response()->json([
                'status'=>200,
                "A6s"=>$A6s
            ],200);
        }
        return response()->json([
            'status'=>404,
            "A6s"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "A6s"=>$validator->messages()
                ],422);   
            }else{
                $A6 = A6::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($A6){
                return response()->json([
                    'status'=>200,
                    "A6s"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "A6s"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $A6 = A6::find($id);
        if($A6){
            return response()->json([
                'status'=>200,
                "A6s"=>$A6
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "A6s"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, A6 $A6)
    {
        $A6->update($request->all());
        return response()->json($A6);
    }

    public function destroy(A6 $A6)
    {
        $A6->delete();
        return response()->json('A6 deleted successfully');
    }
}
