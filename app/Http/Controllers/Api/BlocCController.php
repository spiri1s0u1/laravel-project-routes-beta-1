<?php

namespace App\Http\Controllers\Api;

use App\Models\BlocC;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class BlocCController extends Controller
{
    public function index()
    {
        $BlocCs = BlocC::all();

        if($BlocCs->count()>0){
            return response()->json([
                'status'=>200,
                "BlocCs"=>$BlocCs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "BlocCs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "BlocCs"=>$validator->messages()
                ],422);   
            }else{
                $BlocC = BlocC::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($BlocC){
                return response()->json([
                    'status'=>200,
                    "BlocCs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "BlocCs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $BlocC = BlocC::find($id);
        if($BlocC){
            return response()->json([
                'status'=>200,
                "BlocCs"=>$BlocC
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "BlocCs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, BlocC $BlocC)
    {
        $BlocC->update($request->all());
        return response()->json($BlocC);
    }

    public function destroy(BlocC $BlocC)
    {
        $BlocC->delete();
        return response()->json('BlocC deleted successfully');
    }
}
