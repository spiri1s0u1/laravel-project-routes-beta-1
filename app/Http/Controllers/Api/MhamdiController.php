<?php

namespace App\Http\Controllers\Api;

use App\Models\Mhamdi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class MhamdiController extends Controller
{
    public function index()
    {
        $Mhamdis = Mhamdi::all();

        if($Mhamdis->count()>0){
            return response()->json([
                'status'=>200,
                "Mhamdis"=>$Mhamdis
            ],200);
        }
        return response()->json([
            'status'=>404,
            "Mhamdis"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "Mhamdis"=>$validator->messages()
                ],422);   
            }else{
                $Mhamdi = Mhamdi::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($Mhamdi){
                return response()->json([
                    'status'=>200,
                    "Mhamdis"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "Mhamdis"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $Mhamdi = Mhamdi::find($id);
        if($Mhamdi){
            return response()->json([
                'status'=>200,
                "Mhamdis"=>$Mhamdi
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "Mhamdis"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, Mhamdi $Mhamdi)
    {
        $Mhamdi->update($request->all());
        return response()->json($Mhamdi);
    }

    public function destroy(Mhamdi $Mhamdi)
    {
        $Mhamdi->delete();
        return response()->json('Mhamdi deleted successfully');
    }
}
