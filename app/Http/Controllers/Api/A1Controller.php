<?php

namespace App\Http\Controllers\Api;

use App\Models\a1;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class a1Controller extends Controller
{
    public function index()
    {
        $a1s = a1::all();

        if($a1s->count()>0){
            return response()->json([
                'status'=>200,
                "a1s"=>$a1s
            ],200);
        }
        return response()->json([
            'status'=>404,
            "a1s"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "a1s"=>$validator->messages()
                ],422);   
            }else{
                $a1 = a1::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($a1){
                return response()->json([
                    'status'=>200,
                    "a1s"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "a1s"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $a1 = a1::find($id);
        if($a1){
            return response()->json([
                'status'=>200,
                "a1s"=>$a1
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "a1s"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, a1 $a1)
    {
        $a1->update($request->all());
        return response()->json($a1);
    }

    public function destroy(a1 $a1)
    {
        $a1->delete();
        return response()->json('a1 deleted successfully');
    }
}
