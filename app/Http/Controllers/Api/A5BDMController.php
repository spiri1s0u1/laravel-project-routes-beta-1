<?php

namespace App\Http\Controllers\Api;

use App\Models\a5BDM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class a5BDMController extends Controller
{
    public function index()
    {
        $a5BDMs = a5BDM::all();

        if($a5BDMs->count()>0){
            return response()->json([
                'status'=>200,
                "a5BDMs"=>$a5BDMs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "a5BDMs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "a5BDMs"=>$validator->messages()
                ],422);   
            }else{
                $a5BDM = a5BDM::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($a5BDM){
                return response()->json([
                    'status'=>200,
                    "a5BDMs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "a5BDMs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $a5BDM = a5BDM::find($id);
        if($a5BDM){
            return response()->json([
                'status'=>200,
                "a5BDMs"=>$a5BDM
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "a5BDMs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, a5BDM $a5BDM)
    {
        $a5BDM->update($request->all());
        return response()->json($a5BDM);
    }

    public function destroy(a5BDM $a5BDM)
    {
        $a5BDM->delete();
        return response()->json('a5BDM deleted successfully');
    }
}
