<?php

namespace App\Http\Controllers\Api;

use App\Models\SecretariatGeneral;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class SecretariatGeneralController extends Controller
{
    public function index()
    {
        $SecretariatGenerals = SecretariatGeneral::all();

        if($SecretariatGenerals->count()>0){
            return response()->json([
                'status'=>200,
                "SecretariatGenerals"=>$SecretariatGenerals
            ],200);
        }
        return response()->json([
            'status'=>404,
            "SecretariatGenerals"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "SecretariatGenerals"=>$validator->messages()
                ],422);   
            }else{
                $SecretariatGeneral = SecretariatGeneral::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($SecretariatGeneral){
                return response()->json([
                    'status'=>200,
                    "SecretariatGenerals"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "SecretariatGenerals"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $SecretariatGeneral = SecretariatGeneral::find($id);
        if($SecretariatGeneral){
            return response()->json([
                'status'=>200,
                "SecretariatGenerals"=>$SecretariatGeneral
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "SecretariatGenerals"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, SecretariatGeneral $SecretariatGeneral)
    {
        $SecretariatGeneral->update($request->all());
        return response()->json($SecretariatGeneral);
    }

    public function destroy(SecretariatGeneral $SecretariatGeneral)
    {
        $SecretariatGeneral->delete();
        return response()->json('SecretariatGeneral deleted successfully');
    }
}
