<?php

namespace App\Http\Controllers\Api;

use App\Models\SociauCulturel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class SociauCulturelController extends Controller
{
    public function index()
    {
        $SociauCulturels = SociauCulturel::all();

        if($SociauCulturels->count()>0){
            return response()->json([
                'status'=>200,
                "SociauCulturels"=>$SociauCulturels
            ],200);
        }
        return response()->json([
            'status'=>404,
            "SociauCulturels"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "SociauCulturels"=>$validator->messages()
                ],422);   
            }else{
                $SociauCulturel = SociauCulturel::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($SociauCulturel){
                return response()->json([
                    'status'=>200,
                    "SociauCulturels"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "SociauCulturels"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $SociauCulturel = SociauCulturel::find($id);
        if($SociauCulturel){
            return response()->json([
                'status'=>200,
                "SociauCulturels"=>$SociauCulturel
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "SociauCulturels"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, SociauCulturel $SociauCulturel)
    {
        $SociauCulturel->update($request->all());
        return response()->json($SociauCulturel);
    }

    public function destroy(SociauCulturel $SociauCulturel)
    {
        $SociauCulturel->delete();
        return response()->json('SociauCulturel deleted successfully');
    }
}
