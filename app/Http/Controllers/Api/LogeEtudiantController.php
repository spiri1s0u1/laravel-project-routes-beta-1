<?php

namespace App\Http\Controllers\Api;

use App\Models\LogeEtudiant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class LogeEtudiantController extends Controller
{
    public function index()
    {
        $LogeEtudiants = LogeEtudiant::all();

        if($LogeEtudiants->count()>0){
            return response()->json([
                'status'=>200,
                "LogeEtudiants"=>$LogeEtudiants
            ],200);
        }
        return response()->json([
            'status'=>404,
            "LogeEtudiants"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "LogeEtudiants"=>$validator->messages()
                ],422);   
            }else{
                $LogeEtudiant = LogeEtudiant::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($LogeEtudiant){
                return response()->json([
                    'status'=>200,
                    "LogeEtudiants"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "LogeEtudiants"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $LogeEtudiant = LogeEtudiant::find($id);
        if($LogeEtudiant){
            return response()->json([
                'status'=>200,
                "LogeEtudiants"=>$LogeEtudiant
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "LogeEtudiants"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, LogeEtudiant $LogeEtudiant)
    {
        $LogeEtudiant->update($request->all());
        return response()->json($LogeEtudiant);
    }

    public function destroy(LogeEtudiant $LogeEtudiant)
    {
        $LogeEtudiant->delete();
        return response()->json('LogeEtudiant deleted successfully');
    }
}
