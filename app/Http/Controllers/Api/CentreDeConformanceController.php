<?php

namespace App\Http\Controllers\Api;

use App\Models\CentreDeConformance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class CentreDeConformanceController extends Controller
{
    public function index()
    {
        $CentreDeConformances = CentreDeConformance::all();

        if($CentreDeConformances->count()>0){
            return response()->json([
                'status'=>200,
                "CentreDeConformances"=>$CentreDeConformances
            ],200);
        }
        return response()->json([
            'status'=>404,
            "CentreDeConformances"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "CentreDeConformances"=>$validator->messages()
                ],422);   
            }else{
                $CentreDeConformance = CentreDeConformance::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($CentreDeConformance){
                return response()->json([
                    'status'=>200,
                    "CentreDeConformances"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "CentreDeConformances"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $CentreDeConformance = CentreDeConformance::find($id);
        if($CentreDeConformance){
            return response()->json([
                'status'=>200,
                "CentreDeConformances"=>$CentreDeConformance
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "CentreDeConformances"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, CentreDeConformance $CentreDeConformance)
    {
        $CentreDeConformance->update($request->all());
        return response()->json($CentreDeConformance);
    }

    public function destroy(CentreDeConformance $CentreDeConformance)
    {
        $CentreDeConformance->delete();
        return response()->json('CentreDeConformance deleted successfully');
    }
}

