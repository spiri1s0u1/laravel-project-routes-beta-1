<?php

namespace App\Http\Controllers\Api;

use App\Models\LesLangues;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class LesLanguesController extends Controller
{
    public function index()
    {
        $LesLanguess = LesLangues::all();

        if($LesLanguess->count()>0){
            return response()->json([
                'status'=>200,
                "LesLanguess"=>$LesLanguess
            ],200);
        }
        return response()->json([
            'status'=>404,
            "LesLanguess"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "LesLanguess"=>$validator->messages()
                ],422);   
            }else{
                $LesLangues = LesLangues::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($LesLangues){
                return response()->json([
                    'status'=>200,
                    "LesLanguess"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "LesLanguess"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $LesLangues = LesLangues::find($id);
        if($LesLangues){
            return response()->json([
                'status'=>200,
                "LesLanguess"=>$LesLangues
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "LesLanguess"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, LesLangues $LesLangues)
    {
        $LesLangues->update($request->all());
        return response()->json($LesLangues);
    }

    public function destroy(LesLangues $LesLangues)
    {
        $LesLangues->delete();
        return response()->json('LesLangues deleted successfully');
    }
}
