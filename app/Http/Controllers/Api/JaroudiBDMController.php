<?php

namespace App\Http\Controllers\Api;

use App\Models\Jaroudi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class JaroudiController extends Controller
{
    public function index()
    {
        $Jaroudis = Jaroudi::all();

        if($Jaroudis->count()>0){
            return response()->json([
                'status'=>200,
                "Jaroudis"=>$Jaroudis
            ],200);
        }
        return response()->json([
            'status'=>404,
            "Jaroudis"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "Jaroudis"=>$validator->messages()
                ],422);   
            }else{
                $Jaroudi = Jaroudi::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($Jaroudi){
                return response()->json([
                    'status'=>200,
                    "Jaroudis"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "Jaroudis"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $Jaroudi = Jaroudi::find($id);
        if($Jaroudi){
            return response()->json([
                'status'=>200,
                "Jaroudis"=>$Jaroudi
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "Jaroudis"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, Jaroudi $Jaroudi)
    {
        $Jaroudi->update($request->all());
        return response()->json($Jaroudi);
    }

    public function destroy(Jaroudi $Jaroudi)
    {
        $Jaroudi->delete();
        return response()->json('Jaroudi deleted successfully');
    }
}
