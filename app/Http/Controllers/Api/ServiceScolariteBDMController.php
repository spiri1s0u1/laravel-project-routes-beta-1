<?php

namespace App\Http\Controllers\Api;

use App\Models\ServiceScolarite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class ServiceScolariteController extends Controller
{
    public function index()
    {
        $ServiceScolarites = ServiceScolarite::all();

        if($ServiceScolarites->count()>0){
            return response()->json([
                'status'=>200,
                "ServiceScolarites"=>$ServiceScolarites
            ],200);
        }
        return response()->json([
            'status'=>404,
            "ServiceScolarites"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "ServiceScolarites"=>$validator->messages()
                ],422);   
            }else{
                $ServiceScolarite = ServiceScolarite::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($ServiceScolarite){
                return response()->json([
                    'status'=>200,
                    "ServiceScolarites"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "ServiceScolarites"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $ServiceScolarite = ServiceScolarite::find($id);
        if($ServiceScolarite){
            return response()->json([
                'status'=>200,
                "ServiceScolarites"=>$ServiceScolarite
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "ServiceScolarites"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, ServiceScolarite $ServiceScolarite)
    {
        $ServiceScolarite->update($request->all());
        return response()->json($ServiceScolarite);
    }

    public function destroy(ServiceScolarite $ServiceScolarite)
    {
        $ServiceScolarite->delete();
        return response()->json('ServiceScolarite deleted successfully');
    }
}

