<?php

namespace App\Http\Controllers\Api;

use App\Models\CentreInformatique;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class CentreInformatiqueController extends Controller
{
    public function index()
    {
        $CentreInformatiques = CentreInformatique::all();

        if($CentreInformatiques->count()>0){
            return response()->json([
                'status'=>200,
                "CentreInformatiques"=>$CentreInformatiques
            ],200);
        }
        return response()->json([
            'status'=>404,
            "CentreInformatiques"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "CentreInformatiques"=>$validator->messages()
                ],422);   
            }else{
                $CentreInformatique = CentreInformatique::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($CentreInformatique){
                return response()->json([
                    'status'=>200,
                    "CentreInformatiques"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "CentreInformatiques"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $CentreInformatique = CentreInformatique::find($id);
        if($CentreInformatique){
            return response()->json([
                'status'=>200,
                "CentreInformatiques"=>$CentreInformatique
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "CentreInformatiques"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, CentreInformatique $CentreInformatique)
    {
        $CentreInformatique->update($request->all());
        return response()->json($CentreInformatique);
    }

    public function destroy(CentreInformatique $CentreInformatique)
    {
        $CentreInformatique->delete();
        return response()->json('CentreInformatique deleted successfully');
    }
}
