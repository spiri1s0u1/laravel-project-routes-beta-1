<?php

namespace App\Http\Controllers\Api;

use App\Models\a2BDM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class a2BDMController extends Controller
{
    public function index()
    {
        $a2BDMs = a2BDM::all();

        if($a2BDMs->count()>0){
            return response()->json([
                'status'=>200,
                "a2BDMs"=>$a2BDMs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "a2BDMs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:290',
                'besoinInformatique'=> 'required|string|max:290',
                'lentetient'=> 'required|string|max:290',
                'autres'=> 'required|string|max:290',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "a2BDMs"=>$validator->messages()
                ],422);   
            }else{
                $a2BDM = a2BDM::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($a2BDM){
                return response()->json([
                    'status'=>200,
                    "a2BDMs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "a2BDMs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $a2BDM = a2BDM::find($id);
        if($a2BDM){
            return response()->json([
                'status'=>200,
                "a2BDMs"=>$a2BDM
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "a2BDMs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, a2BDM $a2BDM)
    {
        $a2BDM->update($request->all());
        return response()->json($a2BDM);
    }

    public function destroy(a2BDM $a2BDM)
    {
        $a2BDM->delete();
        return response()->json('a2BDM deleted successfully');
    }
}
