<?php

namespace App\Http\Controllers\Api;

use App\Models\BlocCBDM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class BlocCBDMController extends Controller
{
    public function index()
    {
        $BlocCBDMs = BlocCBDM::all();

        if($BlocCBDMs->count()>0){
            return response()->json([
                'status'=>200,
                "BlocCBDMs"=>$BlocCBDMs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "BlocCBDMs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "BlocCBDMs"=>$validator->messages()
                ],422);   
            }else{
                $BlocCBDM = BlocCBDM::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($BlocCBDM){
                return response()->json([
                    'status'=>200,
                    "BlocCBDMs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "BlocCBDMs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $BlocCBDM = BlocCBDM::find($id);
        if($BlocCBDM){
            return response()->json([
                'status'=>200,
                "BlocCBDMs"=>$BlocCBDM
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "BlocCBDMs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, BlocCBDM $BlocCBDM)
    {
        $BlocCBDM->update($request->all());
        return response()->json($BlocCBDM);
    }

    public function destroy(BlocCBDM $BlocCBDM)
    {
        $BlocCBDM->delete();
        return response()->json('BlocCBDM deleted successfully');
    }
}
