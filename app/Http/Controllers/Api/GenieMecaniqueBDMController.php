<?php

namespace App\Http\Controllers\Api;

use App\Models\GenieMecanique;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class GenieMecaniqueController extends Controller
{
    public function index()
    {
        $GenieMecaniques = GenieMecanique::all();

        if($GenieMecaniques->count()>0){
            return response()->json([
                'status'=>200,
                "GenieMecaniques"=>$GenieMecaniques
            ],200);
        }
        return response()->json([
            'status'=>404,
            "GenieMecaniques"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "GenieMecaniques"=>$validator->messages()
                ],422);   
            }else{
                $GenieMecanique = GenieMecanique::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($GenieMecanique){
                return response()->json([
                    'status'=>200,
                    "GenieMecaniques"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "GenieMecaniques"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $GenieMecanique = GenieMecanique::find($id);
        if($GenieMecanique){
            return response()->json([
                'status'=>200,
                "GenieMecaniques"=>$GenieMecanique
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "GenieMecaniques"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, GenieMecanique $GenieMecanique)
    {
        $GenieMecanique->update($request->all());
        return response()->json($GenieMecanique);
    }

    public function destroy(GenieMecanique $GenieMecanique)
    {
        $GenieMecanique->delete();
        return response()->json('GenieMecanique deleted successfully');
    }
}
