<?php

namespace App\Http\Controllers\Api;

use App\Models\a5;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class a5Controller extends Controller
{
    public function index()
    {
        $a5s = a5::all();

        if($a5s->count()>0){
            return response()->json([
                'status'=>200,
                "a5s"=>$a5s
            ],200);
        }
        return response()->json([
            'status'=>404,
            "a5s"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "a5s"=>$validator->messages()
                ],422);   
            }else{
                $a5 = a5::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($a5){
                return response()->json([
                    'status'=>200,
                    "a5s"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "a5s"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $a5 = a5::find($id);
        if($a5){
            return response()->json([
                'status'=>200,
                "a5s"=>$a5
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "a5s"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, a5 $a5)
    {
        $a5->update($request->all());
        return response()->json($a5);
    }

    public function destroy(a5 $a5)
    {
        $a5->delete();
        return response()->json('a5 deleted successfully');
    }
}
