<?php

namespace App\Http\Controllers\Api;

use App\Models\DecarmetBDM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class DecarmetBDMController extends Controller
{
    public function index()
    {
        $DecarmetBDMs = DecarmetBDM::all();

        if($DecarmetBDMs->count()>0){
            return response()->json([
                'status'=>200,
                "DecarmetBDMs"=>$DecarmetBDMs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "DecarmetBDMs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "DecarmetBDMs"=>$validator->messages()
                ],422);   
            }else{
                $DecarmetBDM = DecarmetBDM::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($DecarmetBDM){
                return response()->json([
                    'status'=>200,
                    "DecarmetBDMs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "DecarmetBDMs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $DecarmetBDM = DecarmetBDM::find($id);
        if($DecarmetBDM){
            return response()->json([
                'status'=>200,
                "DecarmetBDMs"=>$DecarmetBDM
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "DecarmetBDMs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, DecarmetBDM $DecarmetBDM)
    {
        $DecarmetBDM->update($request->all());
        return response()->json($DecarmetBDM);
    }

    public function destroy(DecarmetBDM $DecarmetBDM)
    {
        $DecarmetBDM->delete();
        return response()->json('DecarmetBDM deleted successfully');
    }
}


