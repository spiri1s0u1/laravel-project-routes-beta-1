<?php

namespace App\Http\Controllers\Api;

use App\Models\a6BDM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class a6BDMController extends Controller
{
    public function index()
    {
        $a6BDMs = a6BDM::all();

        if($a6BDMs->count()>0){
            return response()->json([
                'status'=>200,
                "a6BDMs"=>$a6BDMs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "a6BDMs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "a6BDMs"=>$validator->messages()
                ],422);   
            }else{
                $a6BDM = a6BDM::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($a6BDM){
                return response()->json([
                    'status'=>200,
                    "a6BDMs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "a6BDMs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $a6BDM = a6BDM::find($id);
        if($a6BDM){
            return response()->json([
                'status'=>200,
                "a6BDMs"=>$a6BDM
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "a6BDMs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, a6BDM $a6BDM)
    {
        $a6BDM->update($request->all());
        return response()->json($a6BDM);
    }

    public function destroy(a6BDM $a6BDM)
    {
        $a6BDM->delete();
        return response()->json('a6BDM deleted successfully');
    }
}
