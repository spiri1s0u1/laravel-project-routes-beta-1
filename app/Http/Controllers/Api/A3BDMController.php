<?php

namespace App\Http\Controllers\Api;

use App\Models\a3BDM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class a3BDMController extends Controller
{
    public function index()
    {
        $a3BDMs = a3BDM::all();

        if($a3BDMs->count()>0){
            return response()->json([
                'status'=>200,
                "a3BDMs"=>$a3BDMs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "a3BDMs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "a3BDMs"=>$validator->messages()
                ],422);   
            }else{
                $a3BDM = a3BDM::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($a3BDM){
                return response()->json([
                    'status'=>200,
                    "a3BDMs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "a3BDMs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $a3BDM = a3BDM::find($id);
        if($a3BDM){
            return response()->json([
                'status'=>200,
                "a3BDMs"=>$a3BDM
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "a3BDMs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, a3BDM $a3BDM)
    {
        $a3BDM->update($request->all());
        return response()->json($a3BDM);
    }

    public function destroy(a3BDM $a3BDM)
    {
        $a3BDM->delete();
        return response()->json('a3BDM deleted successfully');
    }
}
