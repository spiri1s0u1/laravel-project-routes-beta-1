<?php

namespace App\Http\Controllers\Api;

use App\Models\Mathematiques;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class MathematiquesController extends Controller
{
    public function index()
    {
        $Mathematiquess = Mathematiques::all();

        if($Mathematiquess->count()>0){
            return response()->json([
                'status'=>200,
                "Mathematiquess"=>$Mathematiquess
            ],200);
        }
        return response()->json([
            'status'=>404,
            "Mathematiquess"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "Mathematiquess"=>$validator->messages()
                ],422);   
            }else{
                $Mathematiques = Mathematiques::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($Mathematiques){
                return response()->json([
                    'status'=>200,
                    "Mathematiquess"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "Mathematiquess"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $Mathematiques = Mathematiques::find($id);
        if($Mathematiques){
            return response()->json([
                'status'=>200,
                "Mathematiquess"=>$Mathematiques
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "Mathematiquess"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, Mathematiques $Mathematiques)
    {
        $Mathematiques->update($request->all());
        return response()->json($Mathematiques);
    }

    public function destroy(Mathematiques $Mathematiques)
    {
        $Mathematiques->delete();
        return response()->json('Mathematiques deleted successfully');
    }
}

