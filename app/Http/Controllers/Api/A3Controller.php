<?php

namespace App\Http\Controllers\Api;

use App\Models\a3;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class a3Controller extends Controller
{
    public function index()
    {
        $a3s = a3::all();

        if($a3s->count()>0){
            return response()->json([
                'status'=>200,
                "a3s"=>$a3s
            ],200);
        }
        return response()->json([
            'status'=>404,
            "a3s"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "a3s"=>$validator->messages()
                ],422);   
            }else{
                $a3 = a3::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($a3){
                return response()->json([
                    'status'=>200,
                    "a3s"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "a3s"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $a3 = a3::find($id);
        if($a3){
            return response()->json([
                'status'=>200,
                "a3s"=>$a3
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "a3s"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, a3 $a3)
    {
        $a3->update($request->all());
        return response()->json($a3);
    }

    public function destroy(a3 $a3)
    {
        $a3->delete();
        return response()->json('a3 deleted successfully');
    }
}
