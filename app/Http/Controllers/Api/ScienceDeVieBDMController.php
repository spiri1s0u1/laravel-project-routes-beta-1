<?php

namespace App\Http\Controllers\Api;

use App\Models\ScienceDeVie;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class ScienceDeVieController extends Controller
{
    public function index()
    {
        $ScienceDeVies = ScienceDeVie::all();

        if($ScienceDeVies->count()>0){
            return response()->json([
                'status'=>200,
                "ScienceDeVies"=>$ScienceDeVies
            ],200);
        }
        return response()->json([
            'status'=>404,
            "ScienceDeVies"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "ScienceDeVies"=>$validator->messages()
                ],422);   
            }else{
                $ScienceDeVie = ScienceDeVie::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($ScienceDeVie){
                return response()->json([
                    'status'=>200,
                    "ScienceDeVies"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "ScienceDeVies"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $ScienceDeVie = ScienceDeVie::find($id);
        if($ScienceDeVie){
            return response()->json([
                'status'=>200,
                "ScienceDeVies"=>$ScienceDeVie
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "ScienceDeVies"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, ScienceDeVie $ScienceDeVie)
    {
        $ScienceDeVie->update($request->all());
        return response()->json($ScienceDeVie);
    }

    public function destroy(ScienceDeVie $ScienceDeVie)
    {
        $ScienceDeVie->delete();
        return response()->json('ScienceDeVie deleted successfully');
    }
}
