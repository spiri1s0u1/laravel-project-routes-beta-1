<?php

namespace App\Http\Controllers\Api;

use App\Models\achraf;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class achrafController extends Controller
{
    public function index()
    {
        $achrafs = achraf::all();

        if($achrafs->count()>0){
            return response()->json([
                'status'=>200,
                "achrafs"=>$achrafs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "achrafs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "achrafs"=>$validator->messages()
                ],422);   
            }else{
                $achraf = achraf::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($achraf){
                return response()->json([
                    'status'=>200,
                    "achrafs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "achrafs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $achraf = achraf::find($id);
        if($achraf){
            return response()->json([
                'status'=>200,
                "achrafs"=>$achraf
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "achrafs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, achraf $achraf)
    {
        $achraf->update($request->all());
        return response()->json($achraf);
    }

    public function destroy(achraf $achraf)
    {
        $achraf->delete();
        return response()->json('achraf deleted successfully');
    }
}
