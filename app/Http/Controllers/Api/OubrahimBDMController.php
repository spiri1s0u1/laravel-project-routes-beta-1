<?php

namespace App\Http\Controllers\Api;

use App\Models\Oubrahim;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class OubrahimController extends Controller
{
    public function index()
    {
        $Oubrahims = Oubrahim::all();

        if($Oubrahims->count()>0){
            return response()->json([
                'status'=>200,
                "Oubrahims"=>$Oubrahims
            ],200);
        }
        return response()->json([
            'status'=>404,
            "Oubrahims"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "Oubrahims"=>$validator->messages()
                ],422);   
            }else{
                $Oubrahim = Oubrahim::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($Oubrahim){
                return response()->json([
                    'status'=>200,
                    "Oubrahims"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "Oubrahims"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $Oubrahim = Oubrahim::find($id);
        if($Oubrahim){
            return response()->json([
                'status'=>200,
                "Oubrahims"=>$Oubrahim
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "Oubrahims"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, Oubrahim $Oubrahim)
    {
        $Oubrahim->update($request->all());
        return response()->json($Oubrahim);
    }

    public function destroy(Oubrahim $Oubrahim)
    {
        $Oubrahim->delete();
        return response()->json('Oubrahim deleted successfully');
    }
}
