<?php

namespace App\Http\Controllers\Api;

use App\Models\VisChargerAfairesPedagogic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class VisChargerAfairesPedagogicController extends Controller
{
    public function index()
    {
        $VisChargerAfairesPedagogics = VisChargerAfairesPedagogic::all();

        if($VisChargerAfairesPedagogics->count()>0){
            return response()->json([
                'status'=>200,
                "VisChargerAfairesPedagogics"=>$VisChargerAfairesPedagogics
            ],200);
        }
        return response()->json([
            'status'=>404,
            "VisChargerAfairesPedagogics"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "VisChargerAfairesPedagogics"=>$validator->messages()
                ],422);   
            }else{
                $VisChargerAfairesPedagogic = VisChargerAfairesPedagogic::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($VisChargerAfairesPedagogic){
                return response()->json([
                    'status'=>200,
                    "VisChargerAfairesPedagogics"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "VisChargerAfairesPedagogics"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $VisChargerAfairesPedagogic = VisChargerAfairesPedagogic::find($id);
        if($VisChargerAfairesPedagogic){
            return response()->json([
                'status'=>200,
                "VisChargerAfairesPedagogics"=>$VisChargerAfairesPedagogic
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "VisChargerAfairesPedagogics"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, VisChargerAfairesPedagogic $VisChargerAfairesPedagogic)
    {
        $VisChargerAfairesPedagogic->update($request->all());
        return response()->json($VisChargerAfairesPedagogic);
    }

    public function destroy(VisChargerAfairesPedagogic $VisChargerAfairesPedagogic)
    {
        $VisChargerAfairesPedagogic->delete();
        return response()->json('VisChargerAfairesPedagogic deleted successfully');
    }
}
