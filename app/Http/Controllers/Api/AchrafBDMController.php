<?php

namespace App\Http\Controllers\Api;

use App\Models\achrafBDM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class achrafBDMController extends Controller
{
    public function index()
    {
        $achrafBDMs = achrafBDM::all();

        if($achrafBDMs->count()>0){
            return response()->json([
                'status'=>200,
                "achrafBDMs"=>$achrafBDMs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "achrafBDMs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "achrafBDMs"=>$validator->messages()
                ],422);   
            }else{
                $achrafBDM = achrafBDM::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($achrafBDM){
                return response()->json([
                    'status'=>200,
                    "achrafBDMs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "achrafBDMs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $achrafBDM = achrafBDM::find($id);
        if($achrafBDM){
            return response()->json([
                'status'=>200,
                "achrafBDMs"=>$achrafBDM
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "achrafBDMs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, achrafBDM $achrafBDM)
    {
        $achrafBDM->update($request->all());
        return response()->json($achrafBDM);
    }

    public function destroy(achrafBDM $achrafBDM)
    {
        $achrafBDM->delete();
        return response()->json('achrafBDM deleted successfully');
    }
}
