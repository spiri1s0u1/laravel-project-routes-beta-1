<?php

namespace App\Http\Controllers\Api;

use App\Models\FondanDePouvoir;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class FondanDePouvoirController extends Controller
{
    public function index()
    {
        $FondanDePouvoirs = FondanDePouvoir::all();

        if($FondanDePouvoirs->count()>0){
            return response()->json([
                'status'=>200,
                "FondanDePouvoirs"=>$FondanDePouvoirs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "FondanDePouvoirs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "FondanDePouvoirs"=>$validator->messages()
                ],422);   
            }else{
                $FondanDePouvoir = FondanDePouvoir::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($FondanDePouvoir){
                return response()->json([
                    'status'=>200,
                    "FondanDePouvoirs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "FondanDePouvoirs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $FondanDePouvoir = FondanDePouvoir::find($id);
        if($FondanDePouvoir){
            return response()->json([
                'status'=>200,
                "FondanDePouvoirs"=>$FondanDePouvoir
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "FondanDePouvoirs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, FondanDePouvoir $FondanDePouvoir)
    {
        $FondanDePouvoir->update($request->all());
        return response()->json($FondanDePouvoir);
    }

    public function destroy(FondanDePouvoir $FondanDePouvoir)
    {
        $FondanDePouvoir->delete();
        return response()->json('FondanDePouvoir deleted successfully');
    }
}

