<?php

namespace App\Http\Controllers\Api;

use App\Models\LeDoyen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class LeDoyenController extends Controller
{
    public function index()
    {
        $LeDoyens = LeDoyen::all();

        if($LeDoyens->count()>0){
            return response()->json([
                'status'=>200,
                "LeDoyens"=>$LeDoyens
            ],200);
        }
        return response()->json([
            'status'=>404,
            "LeDoyens"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "LeDoyens"=>$validator->messages()
                ],422);   
            }else{
                $LeDoyen = LeDoyen::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($LeDoyen){
                return response()->json([
                    'status'=>200,
                    "LeDoyens"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "LeDoyens"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $LeDoyen = LeDoyen::find($id);
        if($LeDoyen){
            return response()->json([
                'status'=>200,
                "LeDoyens"=>$LeDoyen
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "LeDoyens"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, LeDoyen $LeDoyen)
    {
        $LeDoyen->update($request->all());
        return response()->json($LeDoyen);
    }

    public function destroy(LeDoyen $LeDoyen)
    {
        $LeDoyen->delete();
        return response()->json('LeDoyen deleted successfully');
    }
}
