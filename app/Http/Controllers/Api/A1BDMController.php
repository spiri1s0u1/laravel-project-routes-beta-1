<?php

namespace App\Http\Controllers\Api;

use App\Models\a1BDM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class a1BDMController extends Controller
{
    public function index()
    {
        $a1BDMs = a1BDM::all();

        if($a1BDMs->count()>0){
            return response()->json([
                'status'=>200,
                "a1BDMs"=>$a1BDMs
            ],200);
        }
        return response()->json([
            'status'=>404,
            "a1BDMs"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'Ndordre'=> 'required|string|max:190',
                'Designation'=> 'required|string|max:190',
                'Quantitie'=> 'required|string|max:190',
                'Observation'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "a1BDMs"=>$validator->messages()
                ],422);   
            }else{
                $a1BDM = a1BDM::create([
                    'Ndordre' => $request->Ndordre,
                    'Designation' => $request->Designation,
                    'Quantitie' => $request->Quantitie,   
                    'Observation' => $request->Quantitie,

                ]);
            }if($a1BDM){
                return response()->json([
                    'status'=>200,
                    "a1BDMs"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "a1BDMs"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $a1BDM = a1BDM::find($id);
        if($a1BDM){
            return response()->json([
                'status'=>200,
                "a1BDMs"=>$a1BDM
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "a1BDMs"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, a1BDM $a1BDM)
    {
        $a1BDM->update($request->all());
        return response()->json($a1BDM);
    }

    public function destroy(a1BDM $a1BDM)
    {
        $a1BDM->delete();
        return response()->json('a1BDM deleted successfully');
    }
}
