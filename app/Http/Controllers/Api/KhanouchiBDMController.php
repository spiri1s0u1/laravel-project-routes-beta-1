<?php

namespace App\Http\Controllers\Api;

use App\Models\Khanouchi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class KhanouchiController extends Controller
{
    public function index()
    {
        $Khanouchis = Khanouchi::all();

        if($Khanouchis->count()>0){
            return response()->json([
                'status'=>200,
                "Khanouchis"=>$Khanouchis
            ],200);
        }
        return response()->json([
            'status'=>404,
            "Khanouchis"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "Khanouchis"=>$validator->messages()
                ],422);   
            }else{
                $Khanouchi = Khanouchi::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($Khanouchi){
                return response()->json([
                    'status'=>200,
                    "Khanouchis"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "Khanouchis"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $Khanouchi = Khanouchi::find($id);
        if($Khanouchi){
            return response()->json([
                'status'=>200,
                "Khanouchis"=>$Khanouchi
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "Khanouchis"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, Khanouchi $Khanouchi)
    {
        $Khanouchi->update($request->all());
        return response()->json($Khanouchi);
    }

    public function destroy(Khanouchi $Khanouchi)
    {
        $Khanouchi->delete();
        return response()->json('Khanouchi deleted successfully');
    }
}
