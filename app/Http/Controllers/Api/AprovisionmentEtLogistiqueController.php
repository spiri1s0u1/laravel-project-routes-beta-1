<?php

namespace App\Http\Controllers\Api;

use App\Models\AprovisionmentEtLogistique;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class AprovisionmentEtLogistiqueController extends Controller
{
    public function index()
    {
        $AprovisionmentEtLogistiques = AprovisionmentEtLogistique::all();

        if($AprovisionmentEtLogistiques->count()>0){
            return response()->json([
                'status'=>200,
                "AprovisionmentEtLogistiques"=>$AprovisionmentEtLogistiques
            ],200);
        }
        return response()->json([
            'status'=>404,
            "AprovisionmentEtLogistiques"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "AprovisionmentEtLogistiques"=>$validator->messages()
                ],422);   
            }else{
                $AprovisionmentEtLogistique = AprovisionmentEtLogistique::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($AprovisionmentEtLogistique){
                return response()->json([
                    'status'=>200,
                    "AprovisionmentEtLogistiques"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "AprovisionmentEtLogistiques"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $AprovisionmentEtLogistique = AprovisionmentEtLogistique::find($id);
        if($AprovisionmentEtLogistique){
            return response()->json([
                'status'=>200,
                "AprovisionmentEtLogistiques"=>$AprovisionmentEtLogistique
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "AprovisionmentEtLogistiques"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, AprovisionmentEtLogistique $AprovisionmentEtLogistique)
    {
        $AprovisionmentEtLogistique->update($request->all());
        return response()->json($AprovisionmentEtLogistique);
    }

    public function destroy(AprovisionmentEtLogistique $AprovisionmentEtLogistique)
    {
        $AprovisionmentEtLogistique->delete();
        return response()->json('AprovisionmentEtLogistique deleted successfully');
    }
}
