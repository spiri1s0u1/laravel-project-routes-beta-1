<?php

namespace App\Http\Controllers\Api;

use App\Models\FormationContinue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class FormationContinueController extends Controller
{
    public function index()
    {
        $FormationContinues = FormationContinue::all();

        if($FormationContinues->count()>0){
            return response()->json([
                'status'=>200,
                "FormationContinues"=>$FormationContinues
            ],200);
        }
        return response()->json([
            'status'=>404,
            "FormationContinues"=>"no records"
        ],404);
    }

    public function store(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'fournisseurLeBureau'=> 'required|string|max:190',
                'besoinInformatique'=> 'required|string|max:190',
                'lentetient'=> 'required|string|max:190',
                'autres'=> 'required|string|max:190',
            ]);
            if($validator->fails()){
                return response()->json([
                    'status'=>200,
                    "FormationContinues"=>$validator->messages()
                ],422);   
            }else{
                $FormationContinue = FormationContinue::create([
                    'fournisseurLeBureau' => $request->fournisseurLeBureau,
                    'besoinInformatique' => $request->besoinInformatique,
                    'lentetient' => $request->lentetient,   
                    'autres' => $request->lentetient,

                ]);
            }if($FormationContinue){
                return response()->json([
                    'status'=>200,
                    "FormationContinues"=>'created'
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    "FormationContinues"=>"something went wrong"
                ],500);
            }
    }
    public function show($id)
    {
        $FormationContinue = FormationContinue::find($id);
        if($FormationContinue){
            return response()->json([
                'status'=>200,
                "FormationContinues"=>$FormationContinue
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                "FormationContinues"=>"not found"
            ],404);
        }
    }

    public function update(Request $request, FormationContinue $FormationContinue)
    {
        $FormationContinue->update($request->all());
        return response()->json($FormationContinue);
    }

    public function destroy(FormationContinue $FormationContinue)
    {
        $FormationContinue->delete();
        return response()->json('FormationContinue deleted successfully');
    }
}
