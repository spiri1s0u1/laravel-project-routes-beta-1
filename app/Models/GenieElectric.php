<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GenieElectric extends Model
{
    use HasFactory;
    protected $table = "GenieElectric";

    protected $fillable = ['fournisseurLeBureau', 'besoinInformatique', 'lentetient','materielDenceignementEtLaboratoire', 'autres'];

}
