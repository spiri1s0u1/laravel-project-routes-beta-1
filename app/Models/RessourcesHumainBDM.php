<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RessourcesHumainBDM extends Model
{
    use HasFactory;
    protected $table = 'RessourcesHumainBDM';
    protected $fillable = ['Ndordre', 'Designation', 'Quantitie', 'ObservatSion'];
}
