<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mathematiques extends Model
{
    use HasFactory;
    protected $table = "Mathematiques";

    protected $fillable = ['fournisseurLeBureau', 'besoinInformatique', 'lentetient','materielDenceignementEtLaboratoire', 'autres'];

}
