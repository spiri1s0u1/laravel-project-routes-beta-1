<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VisChargerAfairesPedagogic extends Model
{
    use HasFactory;
    protected $table = "VisChargerAfairesPedagogic";

    protected $fillable = ['fournisseurLeBureau', 'besoinInformatique', 'lentetient', 'autres'];
}
