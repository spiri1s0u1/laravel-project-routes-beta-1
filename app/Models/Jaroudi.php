<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jaroudi extends Model
{
    use HasFactory;
    protected $table = "Jaroudi";

    protected $fillable = ['fournisseurLeBureau', 'besoinInformatique', 'lentetient', 'autres'];
}
