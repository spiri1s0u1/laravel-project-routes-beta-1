<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AchrafBDM extends Model
{
    use HasFactory;
    protected $table = 'AchrafBDM';
    protected $fillable = ['Ndordre', 'Designation', 'Quantitie', 'ObservatSion'];
}
