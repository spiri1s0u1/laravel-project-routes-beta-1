<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class A3 extends Model
{
    use HasFactory;
    protected $table = "A3";
    protected $fillable = ['fournisseurLeBureau', 'besoinInformatique', 'lentetient', 'autres'];
}
