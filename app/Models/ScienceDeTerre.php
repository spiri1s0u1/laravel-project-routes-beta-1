<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScienceDeTerre extends Model
{
    use HasFactory;
    protected $table = "ScienceDeTerre";

    protected $fillable =['fournisseurLeBureau', 'besoinInformatique', 'lentetient','materielDenceignementEtLaboratoire', 'autres'];

}
