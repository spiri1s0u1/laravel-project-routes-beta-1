<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BureauDordre extends Model
{
    use HasFactory;
    protected $table = "BureauDordre";
    protected $fillable = ['fournisseurLeBureau', 'besoinInformatique', 'lentetient', 'autres'];
}

