<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogeAdministrationBDM extends Model
{
    use HasFactory;
    protected $table = 'LogeAdministrationBDM';
    protected $fillable = ['Ndordre', 'Designation', 'Quantitie', 'ObservatSion'];
}
