<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LesLangues extends Model
{
    use HasFactory;
    protected $table = "LesLangues";

    protected $fillable = ['fournisseurLeBureau', 'besoinInformatique', 'lentetient','materielDenceignementEtLaboratoire', 'autres'];

}
