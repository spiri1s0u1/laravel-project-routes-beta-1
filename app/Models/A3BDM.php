<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class A3BDM extends Model
{
    use HasFactory;
    protected $table = 'A3BDM';
    protected $fillable = ['Ndordre', 'Designation', 'Quantitie', 'ObservatSion'];
}
