<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VisChargerAfairesPedagogicBDM extends Model
{
    use HasFactory;
    protected $table = 'VisChargerAfairesPedagogicBDM';
    protected $fillable = ['Ndordre', 'Designation', 'Quantitie', 'ObservatSion'];
}
