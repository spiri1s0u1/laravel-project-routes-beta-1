<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Decarmet extends Model
{
    use HasFactory;
    protected $table = "Decarmet";

    protected $fillable = ['fournisseurLeBureau', 'besoinInformatique', 'lentetient', 'autres'];
}
