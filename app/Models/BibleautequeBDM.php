<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BibleautequeBDM extends Model
{
    use HasFactory;
    protected $table = 'BibleautequeBDM';
    protected $fillable = ['Ndordre', 'Designation', 'Quantitie', 'ObservatSion'];
}
